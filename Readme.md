# Contracts

Paket geliştirmede kullanılan contract'ların listesi.

## Contracts
- [`AdminContract`](#admincontract)


## AdminContract

`admincontract`, adminlerin listesini ve adminlerin kontrolünü sağlayan methodları tutar.

### içindekiler
- `getAdminList()`
- `isAdmin()`

#### getAdminList

`getAdminList()` methodu admin listesini döndürür. Bu durumda paket geliştirmede bir temele oturup, kararlı ve kesin kodlar yazmamızı sağlar.

Örnek:
```php
    // app/Models/User.php

    use Digitalcake\Contracts\AdminContract;
    use Illuminate\Database\Eloquent\Collection;
    use Illuminate\Database\Eloquent\Factories\HasFactory;
    use Illuminate\Foundation\Auth\User as Authenticatable;
    use Illuminate\Notifications\Notifiable;
    use Laravel\Sanctum\HasApiTokens;

    class User extends Authenticatable implements AdminContract
    {
        use HasFactory;
        use HasApiTokens;
        use Notifiable;

        public static function getAdminList(): Collection
        {
            return $this->where('is_admin', true)->get();
        }
    }
```

#### isAdmin

`isAdmin()` methodu kullanıcının admin olup olmadığını kontrol eder.

Örnek:
```php
    // app/Models/User.php

    use Digitalcake\Contracts\AdminContract;
    use Illuminate\Database\Eloquent\Collection;
    use Illuminate\Database\Eloquent\Factories\HasFactory;
    use Illuminate\Foundation\Auth\User as Authenticatable;
    use Illuminate\Notifications\Notifiable;
    use Laravel\Sanctum\HasApiTokens;

    class User extends Authenticatable implements AdminContract
    {
        use HasFactory;
        use HasApiTokens;
        use Notifiable;

        public function isAdmin(): bool
        {
            return $this->role === 'admin';
        }
    }
```

