<?php

namespace Digitalcake\Contracts;

interface AdminContract
{
    /**
     * Uygulama içerisindeki adminlerin listesini dönmesi gerekli.
     * Örneğin, bu kullanıcılar rol yoluyla belirtilebilir.
     *  return User::where('role', 'admin')->get();
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public static function getAdminList(): \Illuminate\Database\Eloquent\Collection;

    /**
     * Aktif kullanıcı admin mi diye kontrol eder.
     *
     * @return bool
     */
    public function isAdmin(): bool;
}
